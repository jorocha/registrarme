# language: en

Feature: Registrarme y darme de alta en el sitio

  Scenario: El usuario se registra y queda listo para ingresar
    Given que no existe el usuario maria@maria.com
    When intento registrarme como maria@maria.com
    Then me encuentro en login

  Scenario: El usuario no logra registrarse y puede reintentar
    Given que ya existe el usuario pedro@pedro.com
    When intento registrarme como pedro@pedro.com
    Then me encuentro en nuevo-usuario
    And muestra el mensaje 'El usuario ya existe'

  Rule: Solo puede registrarse si el usuario no existe

    Scenario: Si el usuario no existe en el sitio, se da de alta
      Given que no existe el usuario maria@maria.com
      When intento registrarme como maria@maria.com
      Then el usuario se crea

    Scenario: Si el usuario ya existe en el sitio, no se da de alta
      Given que ya existe el usuario pedro@pedro.com
      When intento registrarme como pedro@pedro.com
      Then muestra el mensaje 'El usuario ya existe'

  Rule: El usuario debe ser un email con formato válido

    Scenario: Si el formato de usuario es incorrecto no se da de alta
      Given
      When intento registrarme como pedro.com
      Then el usuario no está registrado
      And muestra el mensaje 'El formato del usuario no es una direccion de email válida'

  Rule: La clave debe contener 2 mayúsculas, 1 catacter especial, 2 numeros y 3 letras minúsculas

    Scenario: Si la clave no contiene 2 mayúsculas, no se registra
      When intento registrarme con clave 12aaaaa!!
      Then el usuario no está registrado
      And muestra el mensaje 'La clave no es segura'
    
    Scenario: Si la clave contiene 2 mayúsculas, 1 catacter especial, 2 numeros y 3 letras minúsculas, se registra
      When intento registrarme con clave 12aaaAA!!
      Then el usuario se crea